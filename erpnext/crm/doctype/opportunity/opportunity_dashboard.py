from frappe import _


def get_data():
	return {
		"fieldname": "opportunity",
		"transactions": [
			{"items": ["Quotation", "Supplier Quotation"]},
		],
	}
